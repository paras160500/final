//
//  CustomSegue.swift
//  This is Final
//
//  Created by Paras Patel on 29/12/18.
//  Copyright © 2018 Paras Patel. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {

    override func perform() {
        scale()
    }
    
    func scale() {
        
        let toViewController = self.destination
        let fromViewController = self.source
        
        let containerView = fromViewController.view.superview
        let originalCenter = fromViewController.view.center
        
        toViewController.view.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        toViewController.view.center = originalCenter
        
        containerView?.addSubview(toViewController.view)
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
            toViewController.view.transform = .identity
        }) { (true) in
            fromViewController.present(toViewController, animated: false, completion: nil)
        }
    }
    
}
