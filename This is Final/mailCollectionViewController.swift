//
//  mailCollectionViewController.swift
//  This is Final
//
//  Created by Paras Patel on 31/12/18.
//  Copyright © 2018 Paras Patel. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class mailCollectionViewController: UICollectionViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

//    let array  = [CollectionViewDataModel.init(typeOfKW: .space, image: UIImage(named: "space")!),CollectionViewDataModel.init(typeOfKW: .digital, image: UIImage(named: "digital")!) ,CollectionViewDataModel.init(typeOfKW: .human, image: UIImage(named: "human")!) ,CollectionViewDataModel.init(typeOfKW: .earth, image: UIImage(named: "earth")!) , CollectionViewDataModel.init(typeOfKW: .city, image: UIImage(named: "city")!) , CollectionViewDataModel.init(typeOfKW: .worldHistory, image: UIImage(named: "worldHistory")!) , CollectionViewDataModel.init(typeOfKW: .automobile, image: UIImage(named: "automobile")!) , CollectionViewDataModel.init(typeOfKW: .sports, image: UIImage(named: "sports")!) , CollectionViewDataModel.init(typeOfKW: .motivation, image: UIImage(named: "motivation"))]
    
    let array = [CollectionViewDataModel.init(typeOfKW: .space, image: UIImage(named: "space")!, nameofType: "SPACE") , CollectionViewDataModel.init(typeOfKW: .digital, image: UIImage(named: "digital")!, nameofType: "Technology") , CollectionViewDataModel.init(typeOfKW: .human, image: UIImage(named: "human"), nameofType: "HUMAN BODY") , CollectionViewDataModel.init(typeOfKW: .earth, image: UIImage(named: "earth"), nameofType: "EARTH") , CollectionViewDataModel.init(typeOfKW: .city, image: UIImage(named: "city")!, nameofType: "CITIES") , CollectionViewDataModel.init(typeOfKW: .worldHistory, image: UIImage(named: "worldHistory")!, nameofType: "WORLD-HISTORY") , CollectionViewDataModel.init(typeOfKW: .automobile, image: UIImage(named: "automobile")!, nameofType: "AUTO MOBILE") , CollectionViewDataModel.init(typeOfKW: .sports, image: UIImage(named: "sports")!, nameofType: "SPORTS") , CollectionViewDataModel.init(typeOfKW: .motivation, image: UIImage(named: "motivation")!, nameofType: "MOTIVATION")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let patternImage = UIImage(named: "ttt2") {
            view.backgroundColor = UIColor(patternImage: patternImage)
        }
        collectionView?.backgroundColor = .clear
        collectionView?.decelerationRate = .fast
        
        
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
       
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return array.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mainCollectionViewCell
        cell.imageView.image = array[indexPath.row].image
        cell.nameLabel.text = array[indexPath.row].nameofType
    
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.heightAnchor.constraint(equalToConstant: UltravisualLayoutConstants.Cell.featuredHeight).isActive = true 
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            cell?.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            cell?.transform = .identity
        }) { (true) in
            print(indexPath)
            
        }
        
    }

}
