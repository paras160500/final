//
//  firstView.swift
//  This is Final
//
//  Created by Paras Patel on 28/12/18.
//  Copyright © 2018 Paras Patel. All rights reserved.
//

import UIKit

@IBDesignable class firstView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 0
        {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderRadius : CGFloat = 0
        {
        didSet {
            layer.borderWidth = borderRadius
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear
        {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
}
