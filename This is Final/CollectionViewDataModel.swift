import UIKit

enum typeofKnowledge
{
    case digital
    case human
    case space
    case earth
    case city
    case automobile
    case film
    case sports
    case motivation
    case worldHistory
}


struct CollectionViewDataModel
{
    var typeOfKW : typeofKnowledge
    var image : UIImage?
    var nameofType : String
}
