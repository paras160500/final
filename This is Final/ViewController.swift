//
//  ViewController.swift
//  This is Final
//
//  Created by Paras Patel on 28/12/18.
//  Copyright © 2018 Paras Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var profileImageContainingView: firstView!
    @IBOutlet weak var trialLanel: UILabel!
    @IBOutlet weak var detailLAbel: UILabel!

    @IBOutlet weak var toppestView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var firstArrow: UIButton!
    @IBOutlet weak var secondArrow: UIButton!
    @IBOutlet weak var thirdArrow: UIButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageContainingView.center = self.view.center
        stackView.center.x = self.view.center.x
        stackView.center.y = self.view.frame.maxY - 40

        animatingTheImage()
    }
    
    override func viewDidLoad() {
       
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender : )))
        upSwipe.direction = .up
        
        self.toppestView.addGestureRecognizer(upSwipe)
    }
    
    @objc func handleSwipe(sender : UISwipeGestureRecognizer)
    {
        if sender.state == .ended
        {
            switch sender.direction {
            case .up:
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                    self.stackView.center.y -= 30
                }) { (true) in
                    self.stackView.center.y += 30
                        self.performSegue(withIdentifier: "SwipeUPSegue", sender: self)
                    
                }
                
            default:
                break
            }
        }
    }
    
    @IBAction func clickme(_ sender: Any) {
        print("Hello PAras")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
        {
        return .lightContent
    }

    func animatingTheImage()
    {
        trialLanel.alpha = 0
        detailLAbel.alpha = 0
        headingLabel.alpha = 0
        
        headingLabel.center.y = -20
        profileImageContainingView.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 1, delay: 0.5, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            self.headingLabel.alpha = 1
            self.headingLabel.center.y = 130
            self.headingLabel.center.x = self.view.center.x
           
            self.profileImageContainingView.layoutIfNeeded()
            self.profileImageContainingView.transform = .identity
            
        }) { (true) in
            self.trialLanel.center.x = self.view.center.x
            self.trialLanel.center.y = self.view.center.y
            
            UIView.animate(withDuration: 0.2, animations: {
                self.trialLanel.alpha = 1
                self.trialLanel.center.y = self.view.center.y + 100
            }, completion: { (true) in
                self.detailLAbel.center.y = self.trialLanel.center.y
                self.detailLAbel.center.x = self.trialLanel.center.x
                UIView.animate(withDuration: 0.3, delay: 0.4, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
                    self.detailLAbel.alpha = 1
                    self.detailLAbel.center.y = self.trialLanel.center.y + 40
                }, completion: { (true) in
                    
                    UIView.animate(withDuration: 1.5, delay: 0, options: [], animations: {
                        
                        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat], animations: {
                            self.firstArrow.tintColor = UIColor.white
                            self.firstArrow.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat], animations: {
                                self.secondArrow.tintColor = UIColor.white
                                self.secondArrow.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                                UIView.animate(withDuration: 0.5, delay: 1, options: [.repeat], animations: {
                                    self.thirdArrow.tintColor = UIColor.white
                                    self.thirdArrow.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                                })
                            })
                        })
                    })
                })
            })
            
        }
        self.profileImageContainingView.setNeedsLayout()
        self.view.isUserInteractionEnabled = true
    }
}

